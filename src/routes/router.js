const express = require("express");
const router = express.Router();
const material = require("../controllers/materialController");
const subject = require("../controllers/subjectController");
const controller = require("../controllers/authController");
const children = require("../controllers/childCintroller");
const tracking = require("../controllers/trackingController");
const tutor = require("../controllers/tutorController");

var checkAuthentication = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect("/login");
  }
};
//auth Routing
router.post("/register", controller.register);
router.post("/login", controller.login);

router.get("/login", controller.getLoginPage);
router.get("/", controller.getHomepage);
router.get("/register", controller.getregister)

// parents section
router.get("/parents-dashboard",checkAuthentication, children.getchild);
router.get("/child-data",checkAuthentication, controller.getChildData);
router.get("/latest-offer", controller.getLatestOffer);
router.get("/offer-detail", controller.getOfferDetail);
router.get("/child-report", controller.getBehaviouralDataReport);


router.post("/child", checkAuthentication,children.create);
router.get("/material", material.index);
;


router.get("/home", controller.getregister);

router.get("/tracking", tracking.index);
router.post("/tracking", tracking.create);
router.delete("/tracking/:id", tracking.delete);

router.get("/subject/", subject.index);
router.post("/subject/create", subject.create);
router.patch("/subject/:id", subject.update);
router.delete("/subject/:id", subject.delete);

// router.get("/student", student.index);
// router.post("/student/create", student.create);
// router.put("/student/update/:id", student.update);
// router.delete("/student/delete/:id", student.delete);

module.exports = router;
