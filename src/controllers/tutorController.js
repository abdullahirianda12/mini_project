const user = require("../models");

class Tutor {
  //get porfile tutor
  static getDataTutor = (req, res) => {
    const tutorId = req.params.id;
    user
      .findOne({ where: { id: tutorId } })
      .then((response) => {
        console.log(response);
        res.json({
          status: 201,
          data: response,
        });
      })
      .catch((e) => {
        res.json({
          status: 422,
          message: "Error get Tutor",
        });
      });
  };

  // update data pribadi tutor
  static update = (req, res) => {
    const tutorDetail = req.params.id;
    const { jeniskelamin, tanggallahir, provinsi, gaji, deskripsi, phonenumber } = req.body;
    user
      .update(
        {
          jeniskelamin,
          tanggallahir,
          provinsi,
          gaji,
          deskripsi,
          phonenumber,
        },
        {
          where: { id: tutorDetail },
        }
      )
      .then((pembimbing) => {
        res.json({
          status: 201,
          data: pembimbing,
        });
      })
      .catch((err) => {
        res.json({
          status: 422,
          message: "gagal update data diri",
        });
      });
  };
}

module.exports = Tutor;
