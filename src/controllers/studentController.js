const {child, student} = require("../models")


class studentcont {
    static index = (req, res)=>{
      student.findAll({
          include:[{
              model: child
          }],
      })
      .then((students)=>{
          res.json({
              data:students,
              message: "This is the data",
          })
      })
      .catch((err)=>{
          res.json({
              status: 500,
              message: err,
          })
      })
    }

    static create = (req,res) => {
        const { serviceId, childId} = req.body
        models.student.create({ serviceId, childId})
        .then((murid) => {
            res.json({
                status: 200,
                message:"Create Succes !",
                data: murid
            })
        })
        .catch((err)=> {
            res.json({
                status: 500,
                message: err
            })
        })
    }

    static update = (req,res) => {
        const studentsid = req.params.id
        const {serviceId, childId} = req.body
        models.student
        .update({
            serviceId,
            childId
        },
        {
            where: {id: studentsid}
        }).then(() =>{
            res.json({
                status: 200,
                data: {serviceId, childId},
                message: "Updated Succes !"
            })
        })
        .catch((err) => {
            res.json({
                status: 500,
                message: err
            })
        })
    }

    static delete =(req, res) => {
        const studentsId = req.params.id
        models.student
        .destroy({
            where: {
                id: studentsId
            }
        })
        .then(()=>{
            res.json({
                status: 200,
                message: "Delete Succes !"
            })
        })
    }

}

module.exports = studentcont
