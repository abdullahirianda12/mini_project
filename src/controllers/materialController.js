const { material, tracking, child, service } = require("../models");

class materialcontroller {
  static index = (req, res) => {
    tracking
      .findAll({})
      .then((response) => {
        res.json({
          data: response,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: err,
        });
      });
  };

  static create = (req, res) => {
    const { name, title, description, subjectId } = req.body;
    material
      .create({ name, title, description, subjectId })
      .then((response) => {
        res.json({
          status: 200,
          message: "Pass",
          data: response,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Server Error",
        });
      });
  };

  static update = (req, res) => {
    const learningId = req.params.id;
    const { name, title, description, subjectId } = req.body;
    material
      .update(
        {
          name,
          title,
          description,
          subjectId,
        },
        {
          where: { id: learningId },
        }
      )
      .then((response) => {
        res.json({
          status: 201,
          data: response,
        });
      })
      .catch((err) => {
        res.json({
          status: 422,
          message: "Can't Update Learning",
        });
      });
  };

  static delete = (req, res) => {
    const learningId = req.params.id;
    material
      .destroy({
        where: {
          id: learningId,
        },
      })
      .then(() => {
        res.json({
          status: 200,
          message: "Learning Deleted",
        });
      });
  };
}
module.exports = materialcontroller;
