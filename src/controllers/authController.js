const { user } = require("../models");
const bcrypt = require("bcrypt");
var passport = require("passport");
const { render } = require("ejs");
const { use } = require("passport");
const role = {
  ORANGTUA : "ORANGTUA",
  TUTOR : "TUTOR"
}
class Auth {
  static register(req, res) {
    console.log(req.body);
  
    const email = req.body.email;
    const username = req.body.username;
    const pw1 = req.body.password;
    const password_confirm = req.body.password_confirm;
    const role = req.body.role;

    if (password_confirm != pw1) {
      console.log("error");
    } else {
      const password = bcrypt.hashSync(pw1, 10);
      user
        .create({
          email: email,
          username: username,
          password: password,
          role : role
        })
        .then((user) => {
          console.log(user);
          res.redirect("/login");
        })
        .catch((err) => {
          console.log(err);
          res.render("register");
       
        });
    }
  }
  static getregister(req, res) {
    res.render("register");
  }

  static getHomepage(req, res) {
    res.render("homepage");
  }

  static login(req, res) {
    console.log("login controller");
    console.log(req.body);
    passport.authenticate("local", {
      successRedirect: "/parents-dashboard",
      failureRedirect: "/login",
    })(req, res);
  }
  static getLoginPage(req, res) {
    res.render("login");
  }
  static getdashboard(req, res) {
    res.render("homepage");
  }
  static getregister(req, res) {
    res.render("register");
  }

  static getHomepage(req, res) {
    res.render("homepage");
  }

  static getParentsDashboard(req, res) {
    res.render("pages/dashboard-orang-tua.ejs");
  }

  static getBehaviouralDataReport(req, res) {
    res.render("pages/detail-laporan.ejs");
  }

  static getChildData(req, res) {
    res.render("pages/data-anak.ejs");
  }

  static getOfferDetail(req, res) {
    res.render("pages/detail-penawaran.ejs");
  }
  static getLatestOffer(req, res) {
    res.render("pages/penawaran-terakhir.ejs");
  }


}

module.exports = Auth;
