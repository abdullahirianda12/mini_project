const {user, service, student} = require("../models");
const bcrypt = require("bcrypt");
var passport = require("passport");
const { render } = require("ejs");
const { use } = require("passport");

class Service {
    static create = (req, res) => {
        if (!req.body.name) {
            res.status(400).send({
              message: "Content can not be empty!"
            });
            return;
          }
          const  services = {
            name: req.body.name,
            deskripsi: req.body.deskripsi,
            rating:req.body.rating,
            studentId : req.body.studentId,
            status :    'PENDING',
            userId : req.user.id
          };
          console.log( services);
    
          service.create(services)
            .then(data => {
              res.send(data);
            })
            .catch(err => {
              res.status(500).send({
                message:
                  err.message || "Some error occurred while creating the Tutorial."
              });
            });
        };
    static getAll = (req, res) => {
        service.findAll({
            include:[{
                model: user,
                model: student
            }],
        },  
        {where: { id: id },
        where: { userId : req.user.id}
    })
        .then((services)=>{
            res.json({
                data:services,
                message: "found data",
            })
        })
        .catch((err)=>{
            res.json({
                status: 500,
                message: err,
            })
        })
      }
    static getOne = (req, res) => {
    const  id = req.params.id;

    service.findByPk(services, {
        where: { id: id },
        where: { userId : req.user.id}
    },
    {
        include:[{
            model: user,
            model: student
        }],
    }
    )
        .then(data => {
        res.send(data);
        })
        .catch(err => {
        res.status(500).send({
            message: "Error retrieving Tutorial with id=" + id
        });
        });
    };
      static confirmStatus = (req, res) => {
        const  id = req.params.id;
        const  services = {
            status : req.body.status
          };
            service.update(services, {
                where: { id: id },
                where: { userId : req.user.id}
            })
            .then((data) => {
                res.json({
                    status: 201,
                    data: data,
                });
                })
                .catch((err) => {
                res.json({
                    status: 422,
                    message: "Can't Update Subject",
                });
            });
        
        };
    static declineStatus = (req, res) => {
        const  id = req.params.id;
        const  services = {
            status : req.body.status
          };
        service.destroy(services, {
            where: { id: id },
            where: { userId : req.user.id}
        })
        .then(() => {
            res.json({
                status: 201,
                message : "DECLINE"
            });
            })
        .catch((err) => {
        res.json({
            status: 422,
            message: "Can't Update Subject",
        });
        });
    };
}


module.exports = Service;